#include "serverclass.h"
#include <QtNetwork/QTcpServer>
#include <QDebug>
#include <QtNetwork/QHostAddress>

ServerClass::ServerClass(int port, QObject *parent) :
    QObject(parent)
{
    tcpServer = new QTcpServer(this);
    if(!tcpServer->listen(QHostAddress::Any, port)){
        qDebug() << "Unable to bind to the port. Try to specify another port";
        exit(1);
    }
    connect(this->tcpServer, SIGNAL(newConnection()), this, SLOT(registerPendingConnection()));
}

void ServerClass::registerPendingConnection(){
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, SIGNAL(disconnected()), this, SLOT(slotUserDisconnect()));
    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(slotRead()));
    peers[clientSocket] = "";
    qDebug() << "Peer Connected. Socket: " << clientSocket;
    // sendToClient(clientSocket, "Waiting for nick.\n");
}
void ServerClass::slotUserDisconnect(){
    QTcpSocket *sock = (QTcpSocket*)sender();
    peers.remove(sock);
    qDebug() << "Peer Disconnected. Socket: " << sock;
    sock->deleteLater();
}
void ServerClass::slotRead(){
    QTcpSocket* clSock = (QTcpSocket*)sender();
    while(clSock->canReadLine()){
    QString result = QString::fromUtf8(clSock->readLine()).trimmed();
    blockSize = 0;
    QRegExp filter("^/user:(.*)$");
    if(filter.indexIn(result) != -1){
        QString nick = filter.cap(1);
        peers[clSock] = nick;
        sendToClient(clSock, "Nick registered.\n");
        updateStatus(nick, "online");
    }else{
        qDebug() << "Unparseable message got from client.";
    }
   }
}
void ServerClass::sendToClient(QTcpSocket* sock, QString mes){
    sock->write(mes.toUtf8());
}
void ServerClass::updateStatus(QString user, QString status){
    userStatus[user] = status;
}
