#include <QtCore/QCoreApplication>
#include <QDebug>
#include "serverclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString par1 = argv[1];
    if(par1 == ""){
        qDebug() << "Error: no port specified.";
        qDebug() << "Usage: XGamerServer [port]";
        exit(1);
    }else{
        QString portParse = argv[1];
        int port = portParse.toInt();
        ServerClass *sClass = new ServerClass(port);
    }
    return a.exec();
}
