#ifndef SERVERCLASS_H
#define SERVERCLASS_H

#include <QObject>
#include <QtNetwork/QtNetwork>
#include <QtNetwork/QTcpServer>

class ServerClass : public QObject
{
    Q_OBJECT
public:
    explicit ServerClass(int port = 2456, QObject *parent = 0);

private:
    QTcpServer *tcpServer;
    QMap<QTcpSocket*, QString> peers;
    quint16 blockSize;
    QMap<QString, QString> userStatus;
private slots:
    void registerPendingConnection();
    void slotUserDisconnect();
    void slotRead();
    void sendToClient(QTcpSocket* sock, QString mes);
    void updateStatus(QString user, QString status);
signals:
    
public slots:

};

#endif // SERVERCLASS_H
