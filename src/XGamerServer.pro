#-------------------------------------------------
#
# Project created by QtCreator 2013-03-08T14:58:28
#
#-------------------------------------------------

QT       += core console network

QT       -= gui

TARGET = XGamerServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    serverclass.cpp

HEADERS += \
    serverclass.h
